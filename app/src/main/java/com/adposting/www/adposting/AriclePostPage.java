package com.adposting.www.adposting;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AriclePostPage extends AppCompatActivity {
    //ImageView to display image selected
    ImageView imageView_1,imageView_2,imageView_3,imageView_4;
    int check=0;
    private ProgressBar progressBar;
    Bitmap bitmap_1, bitmap_2, bitmap_3, bitmap_4;
    //edittext for getting the tags input
    EditText editTextTags;
    TextView player_loading_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aricle_post_page);
        setTitle("Add an Ad");
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        imageView_1 =  findViewById(R.id.imageView_1);
        imageView_2 =  findViewById(R.id.imageView_2);
        imageView_3 =  findViewById(R.id.imageView_3);
        imageView_4 =  findViewById(R.id.imageView_4);
        editTextTags =  findViewById(R.id.editTextTags);
        progressBar = findViewById(R.id.progressBar_cyclic);
        player_loading_text = findViewById(R.id.players_loading_text);


        //checking the permission
        //if the permission is not given we will open setting to add permission
        //else app will not open
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            finish();
            startActivity(intent);
            return;
        }


        //adding click listener to button
        findViewById(R.id.buttonUploadImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //if the tags edittext is empty
                //we will throw input error
                if (editTextTags.getText().toString().trim().isEmpty()) {
                    editTextTags.setError("Enter Descrition first");
                    editTextTags.requestFocus();
                    return;
                }else{
                   // Toast.makeText(AriclePostPage.this, "Click ", Toast.LENGTH_SHORT).show();
                    uploadBitmap(bitmap_1,bitmap_2,bitmap_3,bitmap_4);
                }


            }
        });
    }


    public void select_image(View v){
        //if everything is ok we will open image chooser
        check=check+1;
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 100);

    }



    public void show_progress_bar(){
        progressBar.setVisibility(View.VISIBLE);
        player_loading_text.setVisibility(View.VISIBLE);
        }

        public void hide_progress_bar(){
            progressBar.setVisibility(View.GONE);
            player_loading_text.setVisibility(View.GONE);

        }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

            //getting the image Uri
            Uri imageUri = data.getData();
            try {

                if(check==1){
                    bitmap_1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    imageView_1.setImageBitmap(bitmap_1);
                }else if(check==2){
                    bitmap_2 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    imageView_2.setImageBitmap(bitmap_2);
                }else if(check==3){
                    bitmap_3 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    imageView_3.setImageBitmap(bitmap_3);
                }else if(check==4){
                    bitmap_4 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    imageView_4.setImageBitmap(bitmap_4);
                }

//                imageView_3.setImageBitmap(bitmap);
//                imageView_4.setImageBitmap(bitmap);

                //calling the method uploadBitmap to upload image


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void uploadBitmap(final Bitmap bitmap_up_1,final Bitmap bitmap_up_2,final Bitmap bitmap_up_3,final Bitmap bitmap_up_4) {
        //getting the tag from the edittext
        final String tags = editTextTags.getText().toString().trim();
        show_progress_bar();
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, EndPoints.ADS_UPLOAD_URL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            hide_progress_bar();

                            Intent myIntent = new Intent(AriclePostPage.this, Home.class);
                            AriclePostPage.this.startActivity(myIntent);
                            AriclePostPage.this.finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hide_progress_bar();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide_progress_bar();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("tags", tags);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("pic_1", new DataPart("New_1"+imagename + ".png", getFileDataFromDrawable(bitmap_up_1)));
                params.put("pic_2", new DataPart("New_2"+imagename + ".png", getFileDataFromDrawable(bitmap_up_2)));
                params.put("pic_3", new DataPart("New_3"+imagename + ".png", getFileDataFromDrawable(bitmap_up_3)));
                params.put("pic_4", new DataPart("New_4"+imagename + ".png", getFileDataFromDrawable(bitmap_up_4)));
                return params;
            }
        };

        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon action bar is clicked; go to parent activity
                Intent myIntent = new Intent(AriclePostPage.this, Home.class);
                AriclePostPage.this.startActivity(myIntent);
                AriclePostPage.this.finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }



    @Override
    public void onBackPressed() {

        Intent myIntent = new Intent(AriclePostPage.this, Home.class);
        AriclePostPage.this.startActivity(myIntent);
        AriclePostPage.this.finish();



    }


}
