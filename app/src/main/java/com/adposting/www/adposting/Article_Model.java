package com.adposting.www.adposting;

public class Article_Model {

    String image_url_1,image_url_2,image_url_3,image_url_4;
    String title;

    public Article_Model(String image_url_1,String image_url_2,String image_url_3,String image_url_4, String title) {
        this.image_url_1 = image_url_1;
        this.image_url_2 = image_url_2;
        this.image_url_3 = image_url_3;
        this.image_url_4 = image_url_4;

        this.title = title;
    }

    public String get_Image_url_1() {
        return image_url_1;
    }
    public String get_Image_url_2() {
        return image_url_2;
    }
    public String get_Image_url_3() {
        return image_url_3;
    }
    public String get_Image_url_4() {
        return image_url_4;
    }

    public String get_title() {
        return title;
    }


}
