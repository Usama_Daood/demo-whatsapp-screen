package com.adposting.www.adposting;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PageAdapterFeeds extends FragmentPagerAdapter {

    private int numoftab;

    public PageAdapterFeeds(FragmentManager fm, int numoftab) {
        super(fm);
        this.numoftab = numoftab;
    }
    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new AdsFeed();
            case 1:
                return new ArticleFeed();
//            case 2:
//                return new ThirdFragment();
//            case 3:
//                return new FourthFragment();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return numoftab;
    }



}
