package com.adposting.www.adposting;

public class Ads_Model {
    String image_url;
    String title;

    public Ads_Model(String image_url, String title) {
        this.image_url = image_url;
        this.title = title;
    }

    public String get_Image_url() {
        return image_url;
    }

    public String get_title() {
        return title;
    }



}
