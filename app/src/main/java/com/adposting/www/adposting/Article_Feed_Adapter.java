package com.adposting.www.adposting;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class Article_Feed_Adapter extends RecyclerView.Adapter<Article_Feed_Adapter.AdsViewHolder> {

    private List<Article_Model> Ads_Model;

    Context context;
    public Article_Feed_Adapter(List<Article_Model> playersLists,Context context){
//        this.name=name;
//        this.role=role;
//        this.price=price;
//        this.pic=pic;
        this.Ads_Model=playersLists;
        this.context=context;
    }

    @NonNull
    @Override
    public Article_Feed_Adapter.AdsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.custom_ad_layout,viewGroup,false);
        return new Article_Feed_Adapter.AdsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Article_Feed_Adapter.AdsViewHolder playerViewHolder, int i) {

        Article_Model list = Ads_Model.get(i);

//  Log.d("check",""+list.get_Image_url());

        String image_1 =list.get_Image_url_1();
        String image_2 =list.get_Image_url_2();
        String image_3 =list.get_Image_url_3();
        String image_4 =list.get_Image_url_4();
        String title = list.get_title();

        playerViewHolder.player_name.setText(title);

        try {
            Picasso.get()
                    .load(image_1) //extract as User instance method
                    .resize(120, 120)
                    .error(R.drawable.ic_menu_camera)
                    .into(playerViewHolder.image_1);

            Picasso.get()
                    .load(image_2) //extract as User instance method
                    .resize(120, 120)
                    .error(R.drawable.ic_menu_camera)
                    .into(playerViewHolder.image_2);

            Picasso.get()
                    .load(image_3) //extract as User instance method
                    .resize(120, 120)
                    .error(R.drawable.ic_menu_camera)
                    .into(playerViewHolder.image_3);

            Picasso.get()
                    .load(image_4) //extract as User instance method
                    .resize(120, 120)
                    .error(R.drawable.ic_menu_camera)
                    .into(playerViewHolder.image_4);


        } catch (Exception v) {
            //   Toast.makeText(this, ""+v.toString(), Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public int getItemCount() {
        return Ads_Model.size();
    }

    public class AdsViewHolder extends RecyclerView.ViewHolder{

        ImageView image_1,image_2,image_3,image_4;
        TextView player_name, player_role, player_price;

        public AdsViewHolder(@NonNull View itemView) {
            super(itemView);
            image_1 = itemView.findViewById(R.id.image_1);
            image_2 = itemView.findViewById(R.id.image_2);
            image_3 = itemView.findViewById(R.id.image_3);
            image_4 = itemView.findViewById(R.id.image_4);

            player_name = itemView.findViewById(R.id.submission_text);
        }
    }





}
