package com.adposting.www.adposting;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class Ads_Feeds_Adapter extends RecyclerView.Adapter<Ads_Feeds_Adapter.AdsViewHolder> {

    private List<Ads_Model> Ads_Model;

    Context context;
    public Ads_Feeds_Adapter(List<Ads_Model> playersLists,Context context){
//        this.name=name;
//        this.role=role;
//        this.price=price;
//        this.pic=pic;
        this.Ads_Model=playersLists;
        this.context=context;
    }

    @NonNull
    @Override
    public AdsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.custom_post_layout,viewGroup,false);
        return new AdsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdsViewHolder playerViewHolder, int i) {

        Ads_Model list = Ads_Model.get(i);

//  Log.d("check",""+list.get_Image_url());

        String pi =list.get_Image_url();
        String title = list.get_title();
//        playerViewHolder.player_name.setText(title);

//        try {
//            Picasso.get()
//                    .load(pi) //extract as User instance method
//                    .resize(120, 120)
//                    .into(playerViewHolder.player_pic);
//        } catch (Exception v) {
//            //   Toast.makeText(this, ""+v.toString(), Toast.LENGTH_SHORT).show();
//        }


    }

    @Override
    public int getItemCount() {
        return Ads_Model.size();
    }

    public class AdsViewHolder extends RecyclerView.ViewHolder{

        ImageView player_pic, player_role_pic;
        TextView player_name, player_role, player_price;

        public AdsViewHolder(@NonNull View itemView) {
            super(itemView);
            player_pic = itemView.findViewById(R.id.submission_image);
            player_name = itemView.findViewById(R.id.submission_text);
        }
    }


}
