package com.adposting.www.adposting;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent myIntent = new Intent(MainActivity.this, Home.class);
//                myIntent.putExtra("user_id", user_id);
//                myIntent.putExtra("email", email);
//                myIntent.putExtra("user_name", user_name);
//                myIntent.putExtra("token", token);
//                myIntent.putExtra("pic_url", user_pic);

                MainActivity.this.startActivity(myIntent);
                MainActivity.this.finish();



            }
        }, SPLASH_DISPLAY_LENGTH);



    }
}
