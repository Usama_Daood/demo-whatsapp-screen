package com.adposting.www.adposting;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ArticleFeed extends Fragment {

    TextView nodata,player_loading_text;

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    private ProgressBar progressBar;
    private List<Article_Model> News_Lists;

    int statusCode,tournament_id,team_id;

    public ArticleFeed() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ads_feed_fragment, container, false);

        nodata = view.findViewById(R.id.no_data_found_text);
        recyclerView = view.findViewById(R.id.player_recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),1));
//        recyclerView.setAdapter(new Tournament_players_Adapter(name,role,price,pic));

        progressBar = view.findViewById(R.id.progressBar_cyclic);
        player_loading_text = view.findViewById(R.id.players_loading_text);

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getplayers();

        return view;
    }

    public void getplayers(){
        News_Lists = new ArrayList<>();

        progressBar.setVisibility(View.VISIBLE);
        player_loading_text.setVisibility(View.VISIBLE);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

//        playersLists = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(EndPoints.ADS_GET_PICS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("Article Feed", response.toString());
                progressBar.setVisibility(View.GONE);
                player_loading_text.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


                try {
                    JSONObject obj=new JSONObject(response);
                    JSONArray obj_array=obj.getJSONArray("images");

                    for(int i=0;i< obj_array.length();i++){
                        JSONObject objj = obj_array.getJSONObject(i);
                        Article_Model list_items = new Article_Model(
                                objj.getString("image"),
                                objj.getString("image_1"),
                                objj.getString("image_2"),
                                objj.getString("image_3"),
                                objj.getString("tags")
                        );


                        News_Lists.add(list_items);


                    }

                    adapter = new Article_Feed_Adapter(News_Lists, getContext());
                    recyclerView.setAdapter(adapter);

                    //obj.getString("image");
                    //Toast.makeText(getContext(), ""+obj.getString("image"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


//                adapter = new Tournament_players_Adapter(playersLists, getContext());
//                    recyclerView.setAdapter(adapter);
//
//                    progressBar.setVisibility(View.GONE);
//                    player_loading_text.setVisibility(View.GONE);
//
//                    try {
//                        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                    } catch (Exception c) {
//
//                    }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //      Toast.makeText(getContext(), ""+error, Toast.LENGTH_LONG).show();

                progressBar.setVisibility(View.GONE);
                player_loading_text.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                try {
                    statusCode = error.networkResponse.statusCode;
                    Log.d("", "" + statusCode);
                    //  Toast.makeText(getApplicationContext(), error.getMessage()+" Code "+statusCode, Toast.LENGTH_SHORT).show();
                } catch (Exception b) {

                }


                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //This indicates that the reuest has either time out or there is no connection
                    //       CheckNetwork.Check_errors_in_API(getActivity(), statusCode);

                    progressBar.setVisibility(View.GONE);
                    player_loading_text.setVisibility(View.GONE);
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                    Toast.makeText(getContext(), "Time out Error", Toast.LENGTH_SHORT).show();
                } else if (error instanceof AuthFailureError) {
                    //Error indicating that there was an Authentication Failure while performing the request

                    //     CheckNetwork.Check_errors_in_API(getActivity(), statusCode);

                    progressBar.setVisibility(View.GONE);
                    player_loading_text.setVisibility(View.GONE);
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                } else if (error instanceof ServerError) {
                    //Indicates that the server responded with a error response
                    //   CheckNetwork.Check_errors_in_API(getActivity(), statusCode);

                    progressBar.setVisibility(View.GONE);
                    player_loading_text.setVisibility(View.GONE);
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                } else if (error instanceof NetworkError) {

                    // CheckNetwork.Check_errors_in_API(getActivity(), statusCode);

                    progressBar.setVisibility(View.GONE);
                    player_loading_text.setVisibility(View.GONE);
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                    //Indicates that there was network error while performing the request
                } else if (error instanceof ParseError) {

                    //CheckNetwork.Check_errors_in_API(getActivity(), statusCode);

                    progressBar.setVisibility(View.GONE);
                    player_loading_text.setVisibility(View.GONE);
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                    // Indicates that the server response could not be parsed
                }


            }
        }


        );


        int socketTimeout = 3000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);


        RequestQueue re = Volley.newRequestQueue(getContext());
        re.add(stringRequest);


    }






}
